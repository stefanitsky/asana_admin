from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    gid = CharField(
        _("gid"), max_length=200, blank=True, null=True, unique=True, editable=False
    )
    asana_access_token = CharField(_("Asana access token"), max_length=255)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
