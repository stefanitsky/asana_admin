from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from asana_admin.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    fieldsets = (
        ("User", {"fields": ("asana_access_token",)}),
    ) + auth_admin.UserAdmin.fieldsets
    list_display = ["username", "username", "is_superuser"]
    search_fields = ["name"]
    readonly_fields = ("gid",)

    add_form = UserCreationForm
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2", "asana_access_token",),
            },
        ),
    )
