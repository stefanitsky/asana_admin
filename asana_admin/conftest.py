import pytest

from asana_admin.users.models import User
from asana_admin.users.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture()
def user() -> User:
    return UserFactory()


@pytest.fixture()
def create_user():
    def f(**kwargs) -> User:
        return UserFactory(**kwargs)

    return f
