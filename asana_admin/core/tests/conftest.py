import pytest

from asana_admin.core import models
from asana_admin.core.tests import factories


@pytest.fixture()
def workspace() -> models.Workspace:
    return factories.WorkspaceFactory()


@pytest.fixture()
def create_workspace():
    def f(**kwargs) -> models.Workspace:
        return factories.WorkspaceFactory(**kwargs)

    return f


@pytest.fixture()
def project() -> models.Project:
    return factories.ProjectFactory()


@pytest.fixture()
def create_project():
    def f(**kwargs) -> models.Project:
        return factories.ProjectFactory(**kwargs)

    return f


@pytest.fixture()
def task() -> models.Task:
    return factories.TaskFactory()


@pytest.fixture()
def create_task():
    def f(**kwargs) -> models.Task:
        return factories.TaskFactory(**kwargs)

    return f
