import pytest

from asana_admin.core import services

pytestmark = pytest.mark.django_db


@pytest.mark.integration
class TestAsanaService:
    def test_sync_me(self, user):
        services.AsanaService(user).sync_me()

        assert user.gid is not None
        assert user.username is not None

    def test_create_update_delete_project_and_task(
        self, user, create_project, create_workspace, create_task
    ):
        workspace = create_workspace(user=user, gid="1176366878723357")
        project = create_project(workspace=workspace, gid="")
        task = create_task(project=project, assignee=user)
        service = services.AsanaService(user)

        # Test create project
        service.create_project(project)
        project.refresh_from_db()

        assert project.gid is not None

        # Test update project
        project.name = "TEST_RENAMED"
        project.save(update_fields=("name",))
        service.update_project(project)

        # Test create task
        service.create_task(task)
        task.refresh_from_db()

        assert task.gid is not None

        # Test update task
        task.name = "NEW_NAME"
        task.notes = "New notes"
        service.update_task(task)

        # Test delete task
        service.delete_task(task)

        # Test delete project
        service.delete_project(project)
