import pytest

pytestmark = pytest.mark.django_db


class TestWorkspaceModel:
    def test_str(self, workspace):
        assert str(workspace) == workspace.name


class TestProjectModel:
    def test_str(self, project):
        assert str(project) == project.name


class TestTaskModel:
    def test_str(self, task):
        assert str(task) == task.name
