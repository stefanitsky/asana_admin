import factory

from asana_admin.core import models
from asana_admin.users.tests.factories import UserFactory


class AsanaFactory(factory.DjangoModelFactory):
    gid = factory.Faker("pyint")

    class Meta:
        abstract = True


class WorkspaceFactory(AsanaFactory):
    user = factory.SubFactory(UserFactory)
    name = factory.Faker("word")

    class Meta:
        model = models.Workspace


class ProjectFactory(AsanaFactory):
    workspace = factory.SubFactory(WorkspaceFactory)
    name = factory.Faker("word")

    class Meta:
        model = models.Project


class TaskFactory(AsanaFactory):
    project = factory.SubFactory(ProjectFactory)
    name = factory.Faker("word")
    notes = factory.Faker("text")
    assignee = factory.SubFactory(UserFactory)

    class Meta:
        model = models.Task
