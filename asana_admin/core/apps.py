from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "asana_admin.core"
