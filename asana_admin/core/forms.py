from django import forms

from asana_admin.core import models, services


class ProjectForm(forms.ModelForm):
    class Meta:
        model = models.Project
        fields = "__all__"


class CreateProjectForm(ProjectForm):
    class Meta(ProjectForm.Meta):
        fields = ("workspace", "name")

    def save(self, commit=True):
        project = super().save(commit=commit)

        # Remote create for Asana
        services.AsanaService(project.workspace.user).create_project(project)

        return project


class UpdateProjectForm(ProjectForm):
    class Meta(ProjectForm.Meta):
        fields = ("name",)

    def save(self, commit=True):
        project = super().save(commit=commit)

        # Remote update project
        services.AsanaService(project.workspace.user).update_project(project)

        return project


class TaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = "__all__"


class TaskFormSet(forms.BaseInlineFormSet):
    def save_new_objects(self, commit=True):
        saved_instances = super().save_new_objects(commit=commit)

        # Remotely create tasks
        if commit:
            for i in saved_instances:
                services.AsanaService(i.project.workspace.user).create_task(i)

        return saved_instances

    def save_existing_objects(self, commit=True):
        updated_instances = super().save_existing_objects(commit=commit)

        # Remotely update tasks
        if commit:
            for i in updated_instances:
                services.AsanaService(i.project.workspace.user).update_task(i)

        return updated_instances
