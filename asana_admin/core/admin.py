from asana_admin.core import models, forms

from django.contrib import admin


class GetFormClassMixin:
    @classmethod
    def get_form_cls(cls, request, obj=None):
        if obj is None:
            return getattr(cls, "add_form", cls.form)
        elif obj and request.method == "POST":
            return getattr(cls, "update_form", cls.form)
        else:
            return cls.form


class AsanaAdmin(admin.ModelAdmin, GetFormClassMixin):
    readonly_fields = ("gid",)

    def get_form(self, request, obj=None, **kwargs):
        """
        Generic form handler depends on action (create / update / detail)
        """
        defaults = {"form": self.get_form_cls(request, obj)}
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)


@admin.register(models.Workspace)
class WorkspaceAdmin(AsanaAdmin):
    def has_add_permission(self, request):
        return False


class TaskInline(admin.StackedInline):
    form = forms.TaskForm
    formset = forms.TaskFormSet
    model = models.Task
    extra = 1

    def get_readonly_fields(self, request, obj=None):
        fields = super().get_readonly_fields(request, obj=obj)

        if obj:
            return fields + ("project",)

        return fields


@admin.register(models.Project)
class ProjectAdmin(AsanaAdmin):
    form = forms.ProjectForm
    add_form = forms.CreateProjectForm
    update_form = forms.UpdateProjectForm
    inlines = (TaskInline,)

    def get_readonly_fields(self, request, obj=None):
        """
        Returns readonly fields for a model.
        """
        fields = super().get_readonly_fields(request, obj=obj)

        if obj:
            return fields + ("workspace",)

        return fields
