from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.models import UUIDModel

User = get_user_model()


class AsanaModel(UUIDModel):
    gid = models.CharField(
        _("gid"), max_length=200, blank=True, null=True, unique=True, editable=False
    )

    class Meta:
        abstract = True


class Workspace(AsanaModel):
    user = models.ForeignKey(
        User, models.CASCADE, "workspaces", "workspace", verbose_name=_("user")
    )
    name = models.CharField(_("name"), max_length=255)

    class Meta:
        ordering = ("name",)
        verbose_name = _("workspace")
        verbose_name_plural = _("workspaces")
        constraints = (
            models.UniqueConstraint(
                fields=("gid", "user"), name="unique_user_workspace"
            ),
        )

    def __str__(self) -> str:
        return self.name


class Project(AsanaModel):
    workspace = models.ForeignKey(
        Workspace, models.CASCADE, "projects", "project", verbose_name=_("workspace")
    )
    name = models.CharField(_("name"), max_length=255)

    class Meta:
        ordering = ("name",)
        verbose_name = _("project")
        verbose_name_plural = _("projects")

    def __str__(self) -> str:
        return self.name


class Task(AsanaModel):
    # Может быть m2m: https://developers.asana.com/docs/create-a-task
    # В задании указано сделать к проекту, поэтому делаю точно по заданию
    project = models.ForeignKey(
        Project, models.CASCADE, "tasks", "task", verbose_name=_("project")
    )
    name = models.CharField(_("name"), max_length=255)
    notes = models.TextField(_("notes"))
    assignee = models.ForeignKey(
        User, models.CASCADE, "tasks", "task", verbose_name=_("assignee")
    )

    class Meta:
        ordering = ("name",)
        verbose_name = _("task")
        verbose_name_plural = _("tasks")

    def __str__(self) -> str:
        return self.name
