import asana

from asana_admin.core import models
from asana_admin.users import models as user_models


class AsanaService:
    """
    Asana remote service.
    """

    def __init__(self, user: user_models.User):
        self.user = user
        self._access_token = user.asana_access_token
        self.client = asana.Client.access_token(self._access_token)

    def create_project(self, project: models.Project):
        """
        Remotely creates project.
        """
        response = self.client.projects.create_in_workspace(
            project.workspace.gid, params={"name": project.name}
        )
        project.gid = response["gid"]
        project.save(update_fields=("gid",))

    def update_project(self, project: models.Project):
        """
        Remotely updates project.
        """
        self.client.projects.update(project.gid, params={"name": project.name})

    def delete_project(self, project: models.Project):
        """
        Remotely deletes project.
        """
        self.client.projects.delete(project.gid)
        project.delete()

    def sync_me(self):
        """
        Synchronizes current user.
        """
        me = self.client.users.me()

        # Sync user data
        self.user.gid = me["gid"]
        self.user.username = me["name"]
        self.user.save(update_fields=("username", "gid"))

        # Sync workspaces
        for w in me["workspaces"]:
            models.Workspace.objects.update_or_create(
                user=self.user, gid=w["gid"], defaults=dict(name=w["name"])
            )

    def create_task(self, task: models.Task):
        """
        Remotely creates a task.
        """
        data = dict(
            projects=[task.project.gid],
            name=task.name,
            notes=task.notes,
            assignee=task.assignee.gid,
        )
        response = self.client.tasks.create(data)
        task.gid = response["gid"]
        task.save(update_fields=("gid",))

    def update_task(self, task: models.Task):
        """
        Remotely updates a task.
        """
        data = dict(name=task.name, notes=task.notes, assignee=task.assignee.gid)
        self.client.tasks.update(task.gid, data)

    def delete_task(self, task: models.Task):
        """
        Remotely deletes a task.
        """
        self.client.tasks.delete(task.gid)
        task.delete()
