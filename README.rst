Asana Admin
===========

Asana Admin test project

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


:License: Apache Software License 2.0



Как работать?
-------------
Что необходимо перед началом:

* Зарегистрированный аккаунт Asano_ и access_token_ (Personal access tokens -> New Access Token).

* Обязательное наличие хотябы 1го Workspace, так как API не предоставляет возможности для их создания.

* Установленный и запущенный docker-compose.


Для работы с API Asana необходимо сделать следующие шаги:

1. Поднимаем проект::

    $ docker-compose -f local.yml up -d


2. Создаем админа::

    $docker-compose -f local.yml run django --rm python manage.py createsuperuser


Этой командой вас попросит ввести логин, почту (можно пропустить) и пароль, которые потребуются для доступа в админ панель.

3. Переходим на страницу входа в админ панель и вводим данные только что созданного аккаунта – http://127.0.0.1:8000/admin/

4. Открываем "Users" и добавляем свой access_token в форме обновления вашего аккаунта админа, жмем save.

5. После обновления аккаунта админа будут синхронизированы данные о Workspace и вы сможете приступить к работе с проектами и тасками.


.. _Asano: https://app.asana.com/-/login
.. _access_token: https://app.asana.com/0/developer-console

Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy asana_admin

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ pytest

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html





Deployment
----------

The following details how to deploy this application.



Docker
^^^^^^

See detailed `cookiecutter-django Docker documentation`_.

.. _`cookiecutter-django Docker documentation`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html
